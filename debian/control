Source: golang-github-crewjam-saml
Section: golang
Priority: optional
Maintainer: Debian Go Packaging Team <team+pkg-go@tracker.debian.org>
Uploaders: Thorsten Alteholz <debian@alteholz.de>
Build-Depends: debhelper-compat (= 13),
               dh-golang,
               golang-any,
               golang-github-beevik-etree-dev,
               golang-github-crewjam-httperr-dev,
               golang-github-golang-jwt-jwt-dev,
               golang-github-google-go-cmp-dev,
               golang-github-gotestyourself-gotest.tools-dev,
               golang-github-mattermost-xml-roundtrip-validator-dev,
               golang-github-russellhaering-goxmldsig-dev (>= 1.2.0~),
               golang-github-zenazn-goji-dev,
               golang-golang-x-crypto-dev,
               tzdata
Standards-Version: 4.6.2
Homepage: https://github.com/crewjam/saml
Vcs-Browser: https://salsa.debian.org/go-team/packages/golang-github-crewjam-saml
Vcs-Git: https://salsa.debian.org/go-team/packages/golang-github-crewjam-saml.git
XS-Go-Import-Path: github.com/crewjam/saml
Rules-Requires-Root: no
Testsuite: autopkgtest-pkg-go

Package: golang-github-crewjam-saml-dev
Architecture: all
Multi-Arch: foreign
Depends: golang-github-beevik-etree-dev,
         golang-github-crewjam-httperr-dev,
         golang-github-golang-jwt-jwt-dev,
         golang-github-mattermost-xml-roundtrip-validator-dev,
         golang-github-russellhaering-goxmldsig-dev (>= 1.2.0~),
         golang-github-zenazn-goji-dev,
         golang-golang-x-crypto-dev,
         ${misc:Depends}
Description: SAML library for Golang
 This package contains a partial implementation of the SAML standard in
 golang.  SAML is a standard for identity federation, i.e. either allowing
 a third party to authenticate your users or allowing third parties to rely
 on this software to authenticate their users.  In SAML parlance an Identity
 Provider (IDP) is a service that knows how to authenticate users.
 A Service Provider (SP) is a service that delegates authentication
 to an IDP. If you are building a service where users log in with someone
 else's credentials, then you are a Service Provider. This package supports
 implementing both service providers and identity providers.
